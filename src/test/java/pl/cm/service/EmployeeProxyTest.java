package pl.cm.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.runners.MockitoJUnitRunner;
import pl.cm.EmployeeProvider;
import pl.cm.model.Department;
import pl.cm.model.Employee;
import pl.cm.model.EmployeeBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeProxyTest {

    @Mock
    EmployeeService employeeService;


    @InjectMocks
    EmployeeProxy employeeProxy;


    Employee employee1 = new EmployeeBuilder().withDepartment(Department.IT).build();
    Employee employee2 = new EmployeeBuilder().withDepartment(Department.TECHNICAL).build();
    Employee employee3 = new EmployeeBuilder().withDepartment(Department.MARKETING).build();

    List<Employee> employees = new ArrayList<>();


    @Test
    public void findAllEmployeesWithoutIT() {
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);

        Mockito.when(employeeService.findAll()).thenReturn(employees);

        Assertions.assertThat(employeeProxy.findAll()).containsExactly(employee2, employee3);
        Assertions.assertThat(employeeProxy.findAll()).hasSize(2);
    }

    @Test
    public void findDepartmentWithIT() {
        employees.add(employee1);

        Mockito.when(employeeService.findByDepartment(Department.IT)).thenReturn(employees);

        Assertions.assertThat(employeeProxy.findByDepartment(Department.IT)).isEmpty();
    }

}