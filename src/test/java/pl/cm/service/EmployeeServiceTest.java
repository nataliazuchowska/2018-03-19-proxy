package pl.cm.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.junit.runner.RunWith;
import pl.cm.EmployeeProvider;
import pl.cm.model.Department;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    @Mock
    EmployeeProvider employeeProvider;

    private Department department;

    @InjectMocks
    private EmployeeService instance;

    @Test
    public void shouldReturnAllEmployees() {

        Assertions.assertThat(instance.findAll())
                .isEqualTo(employeeProvider.get());
    }

    @Test
    public void shouldReturnAllEmployeesWithDepartment() {

        assertThat(instance.findByDepartment(Department.IT))
                .isEqualTo(employeeProvider.get().stream()
                        .filter(e -> e.getDepartment() == Department.IT).collect(toList()));
    }
}