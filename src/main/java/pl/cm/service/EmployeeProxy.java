package pl.cm.service;

import pl.cm.model.Department;
import pl.cm.model.Employee;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class EmployeeProxy implements EmployeeFinder {

    private EmployeeService employeeService;
    private List<Employee> employees;

    public EmployeeProxy(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public List<Employee> findAll() {
        employees = employeeService.findAll();
        return employees.stream()
                .filter(e -> e.getDepartment() != Department.IT)
                .collect(toList());
    }

    @Override
    public List<Employee> findByDepartment(Department department) {
        employees = employeeService.findAll();
        return employees.stream()
                .filter(e -> e.getDepartment() != Department.IT)
                .collect(toList());
    }
}