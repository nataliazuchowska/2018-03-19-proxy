package pl.cm.service;

import java.util.List;

import pl.cm.model.Department;
import pl.cm.model.Employee;

public interface EmployeeFinder {

  List<Employee> findAll();

  List<Employee> findByDepartment(Department department);

}
