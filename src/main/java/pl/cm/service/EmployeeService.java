package pl.cm.service;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import pl.cm.EmployeeProvider;
import pl.cm.model.Department;
import pl.cm.model.Employee;

public class EmployeeService implements EmployeeFinder {

  private final List<Employee> employees;

  public EmployeeService(final EmployeeProvider employeeProvider) {
    employees = employeeProvider.get();
  }

  public List<Employee> findAll() {
    return new ArrayList<>(employees);
  }

  public List<Employee> findByDepartment(final Department department) {
    return employees.stream()
        .filter(e -> department == e.getDepartment())
        .collect(toList());
  }
}
