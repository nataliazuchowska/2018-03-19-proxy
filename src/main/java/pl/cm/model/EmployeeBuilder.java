package pl.cm.model;

public class EmployeeBuilder {
  private String name;
  private int age;
  private Department department;

  public EmployeeBuilder withName(final String name) {
    this.name = name;
    return this;
  }

  public EmployeeBuilder withAge(final int age) {
    this.age = age;
    return this;
  }

  public EmployeeBuilder withDepartment(final Department department) {
    this.department = department;
    return this;
  }

  public Employee build() {
    return new Employee(name, age, department);
  }
}