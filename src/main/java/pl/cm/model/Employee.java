package pl.cm.model;

public class Employee {
  private String name;
  private int age;
  private Department department;

  public Employee(final String name, final int age, final Department department) {
    this.name = name;
    this.age = age;
    this.department = department;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(final int age) {
    this.age = age;
  }

  public Department getDepartment() {
    return department;
  }

  public void setDepartment(final Department department) {
    this.department = department;
  }
}
