package pl.cm;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import pl.cm.model.Department;
import pl.cm.model.Employee;
import pl.cm.model.EmployeeBuilder;

public class EmployeeProvider {
  public List<Employee> get() {
    final Faker faker = new Faker();
    final Random random = new Random();
    return IntStream.range(0, 30)
        .mapToObj(i -> new EmployeeBuilder())
        .map(b -> b.withAge(random.nextInt(99)))
        .map(b -> b.withName(faker.name().firstName()))
        .map(b -> b.withDepartment(Department.values()[random.nextInt(3)]))
        .map(EmployeeBuilder::build)
        .collect(toList());
  }
}
